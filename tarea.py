# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import requests
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
import json

# class Company(object):
#     def __init__(self, **kwargs):
#         self.name = kwargs.get('name')
#         self.adress = kwargs.get('adress')
#         self.phone = kwargs.get('phone')
#         self.email = kwargs.get('email')
#         self.website = kwargs.get('website')
#         self.url_logo = kwargs.get('url_logo')



def get_companies(url,timeout=30):
	# browser = webdriver.Firefox()
	browser = webdriver.PhantomJS('./phantomjs')
	browser.set_page_load_timeout(timeout)
	browser.get(url)
	links = browser.find_elements_by_tag_name('h2')
	companies = []

	for i in range(len(links)):
		links[i].click()
		soup = BeautifulSoup(browser.page_source)
		company = {}

		company["name"] = soup.find('div',{"id": "TextoH22"}).text

		soup = soup.find('span',{"id": "TextoH14"})
		company["adress"] = soup.find('li',{"style":"padding-bottom: 5px;"}).text

		data = soup.findAll('li',{"style":"padding-bottom: 3px;"})
		company["phone"] = data[0].text
		company["email"] = data[1].text
		company["website"] = data[2].text

		try:
			company["url_logo"] = soup.find('a').find('img')['src']
		except:
			company["url_logo"] = ""
		print (company)
		companies.append(json.dumps(company))
		browser.back()
		links = browser.find_elements_by_tag_name('h2')

	browser.quit()
	return companies


def save_companies(companies):
	data = "\n".join(companies)
	f = open("companies_data",'w')
	f.write(data)
	f.close()


if __name__ == '__main__':
	url = "http://www.directorioadex.com/Web/Dir2/EmpresaAlf.php"

	companies = get_companies(url)
	save_companies(companies)

